<?php

namespace Drupal\encrypted_link_formatter\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\encrypted_link_formatter\Controller\EncryptedFileDownloadController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class EncryptedLinkFormatterSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.private_file_download')) {
      $route->setDefault('_controller', EncryptedFileDownloadController::class . '::download');
    }
    if ($route = $collection->get('system.files')) {
      $route->setDefault('_controller', EncryptedFileDownloadController::class . '::download');
    }
  }

}
