<?php

namespace Drupal\encrypted_link_formatter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\encrypted_link_formatter\LinkCrypter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * System file controller.
 */
class EncryptedFileDownloadController extends ControllerBase {

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The link crypter service.
   *
   * @var \Drupal\encrypted_link_formatter\LinkCrypter
   */
  protected $linkCrypter;

  /**
   * FileDownloadController constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The streamWrapper manager.
   * @param \Drupal\encrypted_link_formatter\LinkCrypter $link_crypter
   *   The linkCrypter service.
   */
  public function __construct(StreamWrapperManagerInterface $streamWrapperManager, LinkCrypter $link_crypter) {
    $this->streamWrapperManager = $streamWrapperManager;
    $this->linkCrypter = $link_crypter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stream_wrapper_manager'),
      $container->get('encrypted_link_formatter.crypter')
    );
  }

  /**
   * Handles private file transfers.
   *
   * Call modules that implement hook_file_download() to find out if a file is
   * accessible and what headers it should be transferred with. If one or more
   * modules returned headers the download will start with the returned headers.
   * If a module returns -1 an AccessDeniedHttpException will be thrown. If the
   * file exists but no modules responded an AccessDeniedHttpException will be
   * thrown. If the file does not exist a NotFoundHttpException will be thrown.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The transferred file as response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the requested file does not exist.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   */
  public function download(Request $request, $scheme = 'private') {
    $target = $request->query->get('file');

    // Crypt links contains $$, so explode on it.
    if (str_contains($target, '$$')) {
      [, $uri] = explode('$$/', $target);
      $target = $this->linkCrypter->decrypt($uri);
    }

    // Merge remaining path arguments into relative file path.
    $uri = $scheme . '://' . $target;
    $queryParams = NULL;

    // Parse target and remove query parameters.
    if (strpos($uri, '?') !== FALSE) {
      [$uri, $queryParams] = explode('?', $uri, 2);
      // Transform query parameters into an array.
      parse_str($queryParams, $queryParams);
    }

    if ($this->streamWrapperManager->isValidScheme($scheme) && is_file($uri)) {
      // Let other modules provide headers and controls access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$uri]);

      foreach ($headers as $result) {
        if ($result == -1) {
          throw new AccessDeniedHttpException();
        }
      }

      // Set the downloaded filename.
      $headers['Content-Disposition'] = 'attachment; filename="' . basename($uri) . '"';

      // Provide the way to alter final $uri and $headers.
      $this->moduleHandler()->invokeAll('alter_file_uri', [
        &$uri,
        $queryParams,
        &$headers,
        $scheme,
      ]);

      if (count($headers)) {
        // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
        // sets response as not cacheable if the Cache-Control header is not
        // already modified. We pass in FALSE for non-private schemes for the
        // $public parameter to make sure we don't change the headers.
        return new BinaryFileResponse($uri, 200, $headers, $scheme !== 'private');
      }

      throw new AccessDeniedHttpException();
    }

    throw new NotFoundHttpException();
  }

}
