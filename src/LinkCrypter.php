<?php

namespace Drupal\encrypted_link_formatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Service description.
 */
class LinkCrypter {

  const CONFIG_NAME = 'encrypted_link_formatter.settings';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The fileSystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs an Example object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The fileSystem service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $fileSystem) {
    $this->configFactory = $config_factory->get(self::CONFIG_NAME);
    $this->fileSystem = $fileSystem;
  }

  /**
   * A helper function to crypt link.
   */
  public function crypt($uri) {
    $seed = $this->configFactory->get('seed');
    $encryptation_type = $this->configFactory->get('enc_types');

    if (str_contains($uri, '://')) {
      [$scheme, $uri] = explode('://', $uri);
    }

    $enc = $encryptation_type ?? 'base64';
    switch ($enc) {
      case 'base64':
        return $scheme . "://$$/" . base64_encode($uri);

      case 'aes-128-cbc':
        $uri = base64_encode(openssl_encrypt($uri, 'AES-128-CBC', $seed, OPENSSL_RAW_DATA, $this->loadIVfile()));
        return $scheme . "://$$/$uri";
    }

  }

  /**
   * A helper function to decrypt link.
   */
  public function decrypt($uri) {
    $seed = $this->configFactory->get('seed');
    $encryptation_type = $this->configFactory->get('enc_types');

    $enc = $encryptation_type ?? 'base64';
    switch ($enc) {
      case 'base64':
        return base64_decode($uri);

      case 'aes-128-cbc':
        $uri = base64_decode($uri);
        return openssl_decrypt($uri, 'AES-128-CBC', $seed, OPENSSL_RAW_DATA, $this->loadIVfile());
    }

  }

  /**
   * A helper function to ensure Directory Exists.
   */
  public function ensureDirExists($uri) {
    // Check if $uri is dir or file.
    $pathinfo = pathinfo($uri);
    if (isset($pathinfo['extension'])) {
      $uri = dirname($uri);
    }
    if (is_file($uri)) {
      $dirname = pathinfo($uri)['dirname'];
    }
    else {
      $dirname = $uri;
    }

    if (!file_exists($dirname)) {
      return mkdir($dirname, 0775, TRUE);
    }

    return TRUE;
  }

  /**
   * A helper function to load Invalidate files.
   */
  protected function loadIvfile() {
    $private_dir = $this->fileSystem->realpath('private://iv');
    if (file_exists("$private_dir/iv.bin")) {
      return file_get_contents("$private_dir/iv.bin");
    }
  }

}
