<?php

namespace Drupal\encrypted_link_formatter\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\encrypted_link_formatter\LinkCrypter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Encrypted link formatter settings for this site.
 */
class CryptSettingsForm extends ConfigFormBase {

  /**
   * The FileSystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The LinkCrypter service.
   *
   * @var \Drupal\encrypted_link_formatter\LinkCrypter
   */
  protected $linkCrypter;

  /**
   * The CacheTagsInvalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The Crypt settings form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config_factory service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The fileSystem service.
   * @param \Drupal\encrypted_link_formatter\LinkCrypter $linkCrypter
   *   LinkCrypter service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   The cacheTagsInvalidator service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $fileSystem, LinkCrypter $linkCrypter, CacheTagsInvalidatorInterface $cacheTagsInvalidator) {
    parent::__construct($config_factory);
    $this->fileSystem = $fileSystem;
    $this->linkCrypter = $linkCrypter;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('encrypted_link_formatter.crypter'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'encrypted_link_formatter_crypt_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['encrypted_link_formatter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['seed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('This key is used to encrypt the link. If AES encryption is used, the minimum length is 16 characters.'),
      '#default_value' => $this->config('encrypted_link_formatter.settings')
        ->get('seed'),
    ];
    $form['enc_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Encryption types'),
      '#attributes' => [
        'name' => 'enc_types',
      ],
      '#options' => [
        'base64' => $this->t('Base64'),
        'aes-128-cbc' => $this->t('Base64 + AES-128-CBC'),
      ],
      '#default_value' => $this->config('encrypted_link_formatter.settings')
        ->get('enc_types'),
    ];
    $form['enc_lifetime'] = [
      '#type' => 'select',
      '#title' => $this->t('Encryption lifetime'),
      '#description' => $this->t('The time that the link will be valid. Once the time has expired and cron has run, the link will be generated again.'),
      '#options' => [
        60 * 60 => $this->t('1 hour'),
        180 * 60 => $this->t('3 hours'),
        360 * 60 => $this->t('6 hours'),
        720 * 60 => $this->t('12 hours'),
        1440 * 60 => $this->t('24 hours'),
      ],
      '#default_value' => $this->config('encrypted_link_formatter.settings')
        ->get('enc_lifetime'),
      '#states' => [
        'visible' => [
          ':input[name="enc_types"]' => [
            'value' => 'aes-128-cbc',
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('seed'))) {
      $form_state->setErrorByName('seed', $this->t('Private key is required.'));
    }
    if ($form_state->getValue('enc_types') === 'aes-128-cbc'
      && strlen($form_state->getValue('seed')) < 16) {
      $form_state->setErrorByName('seed', $this->t('Private key must be at least 16 characters long.'));
    }
    if ($form_state->getValue('enc_types') !== 'base64') {
      if (!$this->generateIV($form_state->getValue('enc_types'))) {
        $form_state->setError($form, $this->t('Unable to generate IV crypt binary.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('encrypted_link_formatter.settings')
      ->set('seed', $form_state->getValue('seed'))
      ->set('enc_types', $form_state->getValue('enc_types'))
      ->set('enc_lifetime', $form_state->getValue('enc_lifetime'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to generate invalidation tags.
   *
   * @param string $type
   *   Type of field.
   *
   * @return bool
   *   TRUE or FALSE according to the configuration settings.
   */
  protected function generateIv($type): bool {
    $private_dir = $this->fileSystem->realpath('private://');
    if (!$this->linkCrypter->ensureDirExists("$private_dir/iv")) {
      return FALSE;
    }
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($type));
    if (!file_put_contents("$private_dir/iv/iv.bin", $iv)) {
      return FALSE;
    }
    $this->cacheTagsInvalidator->invalidateTags(['encrypted_file_download']);
    return TRUE;
  }

}
