# Encrypted link formatter

This Drupal module allows for the encryption of URL's generated for private file downloads. This helps to secure the files and ensure that only authorized individuals can access them, altering the final generated URL.

**Features**
Encryption formatter with the following encryption modes:
Base 64 link encryption
Base 64 link encryption + AES-128-CBC
Auto-regeneration of links by cron (from 1 hour to 24 hours)
A service to encrypt other types of information in your custom modules

For a full description of the module, visit the
[project page](https://www.drupal.org/project/encrypted_link_formatter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/encrypted_link_formatter).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This project requires Drupal core. No additional dependencies are needed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

It is necessary to have the private files directory configured for the module to work properly.


## Maintainers

- Daniel Cimorra - [dcimorra](https://www.drupal.org/u/dcimorra)